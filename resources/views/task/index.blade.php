@extends('layouts.app')

@section('title')
태스크 목록
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <form class="form-horizontal" role="form" method="GET" action="{{ route('task.index') }}">
                <div class="container">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="start_date">시작일</label>
                            <div class="input-group date" id="start_date">
                                <input type="text" name="start_date" class="form-control"><span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="end_date">종료일</label>
                            <div class="input-group date" id="end_date">
                                <input type="text" name="end_date" class="form-control"><span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="priority">우선순위</label>
                            <select name="priority" class="form-control">
                                @foreach(['all', 'low', 'medium', 'high'] as $p)
                                    <option value="{{ $p }}" {{ ($priority===$p) ? "selected":"" }}>{{ $p }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="status">상태</label>
                            <select name="status" class="form-control">
                                @foreach(['all', 'set', 'doing', 'done'] as $s)
                                    <option value="{{ $s }}" {{ ($status === $s) ? "selected" : "" }}>{{ $s }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <div style="margin-top:24px;">
                                <button type="submit" class="btn btn-primary">찾기</button>
                            </div>
                        </div>
                    </div>

                </div>
            </form>

            <h3>태스크 목록 보기</h3>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <td>Project</td>
                        <td>Task</td>
                        <td>Priority</td>
                        <td>Status</td>
                        <td>Due Date</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($tasks as $task)
                        <tr>
                            <td>{{ $task->project->name }}</td>
                            <td><a href="{{ route('project.task.show', [$task->project->id, $task->id]) }}">{{ $task->name }}</a></td>
                            <td>{{ $task->priority }}</td>
                            <td>{{ $task->status }}</td>
                            <td>{{ $task->due_date}}</td>
                            <td><a href="{{ route('project.task.edit', [$task->project->id, $task->id]) }}">편집</a></td>
                            <td>
                                <form method="POST" action="{{ route('project.task.destroy', [$task->project->id, $task->id]) }}">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <button type="submit" class="btn btn-danger">삭제</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>

</div>

@endsection

@section('script')
<script type="text/javascript">
    $(function(){
        $('#start_date').datetimepicker({
            defaultDate:'{{ $start_date }}',
            format:'YYYY-MM-DD HH:mm:ss'
        });
        $('#end_date').datetimepicker({
            defaultDate:'{{ $end_date }}',
            format:'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        });
        $('#start_date').on('dp.change', function(e){
            $('#end_date').data('DateTimePicker').minDate(e.date);
            console.log(e.data);
        });
        $('#end_date').on('dp.change', function(e){
            $('#start_date').data('DateTimePicker').maxDate(e.date);
            console.log(e.data);
        });
    });
</script>
@endsection