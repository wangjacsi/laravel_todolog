@extends('layouts.app')

@section('title')
Welcome Page
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Laravel Todo Log Site!</div>

                <div class="panel-body">
                    <div class="container">
                        Total Users: {{ $total['user'] }}</p>
                        Total Projects: {{ $total['project'] }}</p>
                        Total Tasks: {{ $total['task'] }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
