@extends('layouts.app')

@section('title')
    프로젝트 정보
@endsection

@section('content')
<div class="container">
<div class="row">
    <div class="col-md-8">
        @if(Session::has('message'))
            <div class="alert alert-info">{{ Session::get('message') }}</div>
        @endif
        <h3>프로젝트 정보</h3>
        <div class="form-group">
            <label for="name">프로젝트 명</label>
            <div>
                <input type="text" name="name" class="form-control" value="{{ $proj->name }}" readonly="true">
            </div>
        </div>

        <div class="form-group">
            <label for="description">설명</label>
            <div>
                <textarea class="form-control" rows="3" name="description"  readonly="true">{{ $proj->description }}</textarea>
            </div>
        </div>

        <div class="form-group">
            <label for="created_at">생성일</label>
            <div>
                <input type="text" name="created_at" class="form-control" value="{{ $proj->created_at }}" readonly="true">
            </div>
        </div>

        <div class="form-group">
            <label for="updated_at">수정일</label>
            <div>
                <input type="text" name="updated_at" class="form-control" value="{{ $proj->updated_at }}" readonly="true">
            </div>
        </div>

        <p>
            <a href="{{ route('project.task.index', $proj->id) }}" class="btn btn-info">Task 목록 보기</a>
        </p>

    </div>
</div>
</div>
@endsection