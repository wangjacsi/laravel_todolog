@extends('layouts.app')

@section('title')
    태스크 수정
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @if(count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                <form class="form-horizontal" role="form" method="POST" action="{{ route('project.task.update', [$task->project->id, $task->id]) }}">
                    {{ method_field('PUT') }}
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="project_id">프로젝트 선택</label>
                        <div>
                            <select class="form-control" name="project_id">
                                @foreach ($projects as $proj)
                                    <option value="{{ $proj->id }}" {{ ($task->project->id == $proj->id) ? "selected" : "" }}>{{ $proj->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="name">태스크 명</label>
                        <div>
                            <input type="text" name="name" value="{{ $task->name }}" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="description">설명</label>
                        <div>
                            <textarea class="form-control" rows="3" name="description">{{ $task->description }}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="priority">우선순위</label>
                        <div>
                            <select class="form-control" name="priority">
                                @foreach (['low', 'medium', 'high'] as $p)
                                    <option value="{{ $p }}" {{ ($task->priority === $p) ? "selected" : "" }}>{{ $p }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="status">상태</label>
                        <div>
                            <select name="status" class="form-control">
                                @foreach (['set', 'doing', 'done'] as $s)
                                    <option value="{{ $s }}" {{ ($task->status === $s) ? "selected" : "" }}>{{ $s }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="due_date">기한</label>
                        <div class="input-group date" id="due_date">
                            <input type="text" class="form-control" name="due_date" value="{{ $task->due_date }}">
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>

                    </div>

                    <div class="form-group">
                        <label for="created_at">생성일</label>
                        <div>
                            <input type="text" name="created_at" class="form-control" value="{{ $task->created_at }}" readonly="true">
                        </div>
                    </div>

                    <div class="form-group">
                        <div>
                            <button type="submit" class="btn btn-primary">수정</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(function(){
            $("#due_date").datetimepicker({
                locale:'en',
                defaultDate:'{{ $task->due_date }}',
                format: 'YYYY-MM-D HH:mm:ss'
            });
        });
    </script>
@endsection