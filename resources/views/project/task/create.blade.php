@extends('layouts.app')

@section('title')
    태스크 생성
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @if(count($errors) > 0 )
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors as $e)
                            <li>{{ $e }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form class="form-horizontal" role="form" method="POST" action="{{ route('project.task.store', [$proj->id]) }}">
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="name">태스크 명</label>
                    <div>
                        <input type="text" name="name" class="form-control" value="{{ old('name') }}">
                    </div>
                </div>

                <div class="form-group">
                    <label for="description">설명</label>
                    <div>
                        <textarea class="form-control" rows="3" name="description">{{ old('description') }}</textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label for="priority">우선순위</label>
                    <div>
                        <select class="form-control" name="priority">
                            @foreach (['low', 'medium', 'high'] as $p)
                                <option value="{{ $p }}" {{ (old('priority') === $p) ? "selected" : "" }}>{{ $p }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="status">상태</label>
                    <div>
                        <select class="form-control" name="status">
                            @foreach (['set', 'doing', 'done'] as $s)
                                <option value="{{ $s }}" {{ (old('status') === $s) ? "selected" : "" }}>{{ $s }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="due_date">기한</label>
                    <div class="input-group date" id="due_date">
                        <input type="text" name="due_date" class="form-control">
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>

                <div class="form-group">
                    <div>
                        <button type="submit" class="btn btn-primary">생성</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
    $(function(){
        $('#due_date').datetimepicker({
            locale:'en',
            defaultDate:{!! (old('due_date')) ? old('due_date') : "'". (Carbon\Carbon::now())."'" !!},
            format:'YYYY-MM-DD HH:mm:ss'
        });
    });
</script>
@endsection