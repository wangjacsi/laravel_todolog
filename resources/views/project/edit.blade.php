@extends('layouts.app')

@section('title')
    프로젝트 수정
@endsection

@section('content')
    <div class="container">
    <div class="row">
    <div class="col-md-8">
        <form class="form-horizontal" role="form" method="POST" action="{{ route('project.update', $proj->id) }}">
            {{ method_field('PUT') }}
            {{ csrf_field() }}

            <div class="form-group">
                <label for="name">프로젝트 명</label>
                <div>
                    <input type="text" name="name" value="{{ $proj->name }}" class="form-control">
                </div>
            </div>

            <div class="form-group">
                <label for="description">프로젝트 설명</label>
                <div>
                    <textarea name="description" class="form-control" rows="5">{{ $proj->description }}</textarea>
                </div>
            </div>

            <div class="form-group">
                <label for="created_at">생성일</label>
                <div>
                    <input type="text" name="created_at" class="form-control" readonly="true" value="{{ $proj->created_at }}">
                </div>
            </div>

            <div class="form-group">
                <div>
                    <button class="btn btn-primary" type="submit">수정</button>
                </div>
            </div>

        </form>
    </div>
    </div>
    </div>



@endsection