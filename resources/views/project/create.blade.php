@extends('layouts.app')

@section('title')
프로젝트 생성
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>프로젝트 생성</h1>
            <form class="form-horizontal" method="POST" action="{{ route('project.store') }}" role="form">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="name">프로젝트 명</label>
                    <div>
                        <input type="text" name="name" class="form-control" value="{{ old('name') }}">
                    </div>
                </div>

                <div class="form-group">
                    <label for="description">프로젝트 설명</label>
                    <div>
                        <textarea class="form-control" rows="5" name="description">{{ old('description') }}</textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div>
                        <button type="submit" class="btn btn-primary">프로젝트 생성</button>
                    </div>
                </div>

            </form>

        </div>
    </div>
</div>
@endsection